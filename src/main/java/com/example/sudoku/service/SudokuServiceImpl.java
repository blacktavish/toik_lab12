package com.example.sudoku.service;

import com.example.sudoku.component.CsvReaderComponent;
import com.example.sudoku.dto.SudokuDto;
import com.example.sudoku.mapper.SudokuMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

@Service
public class SudokuServiceImpl implements SudokuService {

    private final CsvReaderComponent csvReaderComponent;
    private final SudokuMapper sudokuMapper;

    @Autowired
    public SudokuServiceImpl(CsvReaderComponent csvReaderComponent, SudokuMapper sudokuMapper) {
        this.csvReaderComponent = csvReaderComponent;
        this.sudokuMapper = sudokuMapper;
    }

    @Override
    public SudokuDto getSudoku(String fileName, String divider) {
        List<String> lines = csvReaderComponent.readCsvFile(fileName, divider);
        return checkSudoku(sudokuMapper.convertLines(lines));
    }

    public SudokuDto checkSudoku(Map<String, List<List<Integer>>> sudoku){
        final SudokuDto errorsList = new SudokuDto();
        for(int row = 0; row < 9; row++) {
            for (int col = 0; col < 8; col++) {
                for (int col2 = col + 1; col2 < 9; col2++) {
                    if (sudoku.get("sudoku").get(row).get(col) == sudoku.get("sudoku").get(row).get(col2)) {
                        errorsList.columnIds.add(col);
                    }
                }
            }
        }

        for(int col = 0; col < 9; col++) {
            for (int row = 0; row < 8; row++) {
                for (int row2 = row + 1; row2 < 9; row2++) {
                    if (sudoku.get("sudoku").get(row).get(col) == sudoku.get("sudoku").get(row2).get(col)) {
                        errorsList.lineIds.add(row);
                    }
                }
            }
        }

        for(int row = 0; row < 9; row += 3) {
            for (int col = 0; col < 9; col += 3) {
                for (int pos = 0; pos < 8; pos++) {
                    for (int pos2 = pos + 1; pos2 < 9; pos2++) {
                        if (sudoku.get("sudoku").get(row + pos % 3).get(col + pos / 3) == sudoku.get("sudoku").get(row + pos2 % 3).get(col + pos2 / 3)) {
                            errorsList.areaIds.add(row+pos%3);
                            errorsList.areaIds.add(col+pos/3);
                        }
                    }
                }
            }
        }
        return errorsList;
    }
}
