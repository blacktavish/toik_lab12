package com.example.sudoku.mapper;

import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Component
public class SudokuMapperImpl implements SudokuMapper{

    @Override
    public Map<String, List<List<Integer>>> convertLines(List<String> lines) {
        List<List<Integer>> sudoku = new ArrayList<>(9);
        for(int i=0; i<9 ;i++){
            sudoku.add(new ArrayList<>());
        }
        for(final String l:lines){
            sudoku.get(0).add(Integer.parseInt(l.split(",")[0]));
            sudoku.get(1).add(Integer.parseInt(l.split(",")[1]));
            sudoku.get(2).add(Integer.parseInt(l.split(",")[2]));
            sudoku.get(3).add(Integer.parseInt(l.split(",")[3]));
            sudoku.get(4).add(Integer.parseInt(l.split(",")[4]));
            sudoku.get(5).add(Integer.parseInt(l.split(",")[5]));
            sudoku.get(6).add(Integer.parseInt(l.split(",")[6]));
            sudoku.get(7).add(Integer.parseInt(l.split(",")[7]));
            sudoku.get(8).add(Integer.parseInt(l.split(",")[8]));
        }
        Map<String, List<List<Integer>>> mapSudoku = new HashMap<>();
        mapSudoku.put("sudoku", sudoku);

        return mapSudoku;
    }
}
